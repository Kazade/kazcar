/*
 * Simple playground app demonstrating the car physics. Uses KGLT
 * http://github.com/kazade/kglt
 */

#include <kglt/kglt.h>
#include <kglt/extra/skybox.h>

#include "kazcar/kazcar.h"

const std::string MESH_FILE = "assets/physics-playground.obj";
const std::string CAR_MESH_FILE = "assets/police_car.obj";

bool step_mode = false;

class Renderer {
public:
    Renderer(kglt::StagePtr stage):
        stage_(stage) {
        mesh_ = stage->new_mesh();
        auto mesh = stage->mesh(mesh_);
        idx_ = mesh->new_submesh(stage->clone_default_material(), kglt::MESH_ARRANGEMENT_TRIANGLES, true);

        actor_ = stage->new_actor_with_mesh(mesh_);
    }

    void update(const kmVec3* vertices, const kmVec3* normals, uint32_t num_vertices, const uint32_t* indices, uint32_t num_indices) {
        auto mesh = stage_->mesh(mesh_);
        mesh->shared_data().clear();
        for(uint32_t i = 0; i < num_vertices; ++i) {
            mesh->shared_data().position(vertices[i]);
            mesh->shared_data().diffuse(kglt::Colour::WHITE);
            mesh->shared_data().normal(normals[i]);
            mesh->shared_data().move_next();
        }

        auto& submesh = mesh->submesh(idx_);
        submesh.index_data().clear();
        for(uint32_t i = 0; i < num_indices; i += 3) {
            auto i1 = indices[i], i2 = indices[i+1], i3 = indices[i+2];

            submesh.index_data().index(i1);
            submesh.index_data().index(i2);
            submesh.index_data().index(i3);
        }

        mesh->shared_data().done();
        submesh.index_data().done();
    }

    static void update_thunk(const kmVec3* vertices, const kmVec3* normals, uint32_t num_vertices, const uint32_t* indices, uint32_t num_indices, void* user_data) {
        Renderer* renderer = static_cast<Renderer*>(user_data);
        return renderer->update(vertices, normals, num_vertices, indices, num_indices);
    }

private:
    kglt::StagePtr stage_;
    kglt::MeshID mesh_;
    kglt::SubMeshIndex idx_;
    kglt::ActorID actor_;
};


class GameScreen : public kglt::Screen<GameScreen> {
public:
    GameScreen(kglt::WindowBase& window):
        kglt::Screen<GameScreen>(window) {}

    void do_load() {
        pipeline_ = prepare_basic_scene(stage_, camera_);

        auto stage = window->stage(stage_);
        stage->host_camera(camera_);

        //Just stash the skybox with the window so we always have access to it
        window->data->stash(kglt::extra::StarField::create(stage, camera_), "skybox");

        world_ = window->new_mesh_from_file(MESH_FILE, /*garbage_collect=*/false);
        car_mesh_ = window->new_mesh_from_file(CAR_MESH_FILE, false);

        // Unit cylinder so we can scale properly per-car
        wheel_mesh_ = window->new_mesh_as_cylinder(1.0, 1.0, 20, 1, false);
        window->mesh(wheel_mesh_)->set_diffuse(kglt::Colour::BLACK);

        kglt::Mat4 rot;
        kmMat4RotationY(&rot, kmDegreesToRadians(180));
        window->mesh(car_mesh_)->normalize();
        window->mesh(car_mesh_)->transform_vertices(rot);
        kmMat4Scaling(&rot, 6.0, 6.0, 6.0);
        window->mesh(car_mesh_)->transform_vertices(rot);

        car_actor_ = stage->new_actor_with_mesh(car_mesh_);

        stage->camera(camera_)->follow(car_actor_, kglt::CAMERA_FOLLOW_MODE_DIRECT, kglt::Vec3(0, 5, 20));

        //stage->camera(camera_)->move_to(0, 5, 10);
       // stage->camera(camera_)->look_at(10, 0, 0);

        auto lid = stage->new_light(kglt::LIGHT_TYPE_DIRECTIONAL);
        stage->set_ambient_light(kglt::Colour(0.5, 0.5, 0.5, 1.0));

        renderer_.reset(new Renderer(window->stage(stage_)));
        physics_world_ = kc_world_create();
        kc_world_set_render_callback(physics_world_, &Renderer::update_thunk, renderer_.get());

        auto world_mesh = window->mesh(world_);

        kglt::Mat4 scale;
        kmMat4Scaling(&scale, 15, 15, 15);
        world_mesh->transform_vertices(scale);

        auto& vertex_data = world_mesh->shared_data();
        auto& index_data = world_mesh->submesh(world_mesh->submesh_ids()[0]).index_data();

        for(uint32_t i = 0; i < index_data.count(); i += 3) {
            auto idx1 = index_data.at(i);
            auto idx2 = index_data.at(i+1);
            auto idx3 = index_data.at(i+2);

            kmVec3 v1, v2, v3;
            // Data slicing is fine here (kglt::Vec3 is a kmVec3 subclass with no extra data)
            v1 = (kmVec3) vertex_data.position_at(idx1);
            v2 = (kmVec3) vertex_data.position_at(idx2);
            v3 = (kmVec3) vertex_data.position_at(idx3);

            kc_world_add_triangle(physics_world_, &v1, &v2, &v3);
        }

        // World generated, let's add a car!
        car_ = kc_car_create(physics_world_);
        kmVec3 position = { 0, 1.0, 0 };
        kc_car_set_position(car_, &position);
        stage->actor(car_actor_)->move_to(position);

        // Scale the wheel mesh to be the same size as the physics engine says
        float wheel_radius = kc_car_wheel_radius(car_);
        kglt::Mat4 scaling;
        kmMat4Scaling(&scaling, wheel_radius * 2, wheel_radius * 0.75, wheel_radius * 2);
        window->mesh(wheel_mesh_)->transform_vertices(scaling);

        // Default cylinder points upwards, rotate to face side (reuse rot variable)
        kmMat4RotationZ(&rot, kmDegreesToRadians(90));
        window->mesh(wheel_mesh_)->transform_vertices(rot);

        for(uint8_t i = 0; i < 4; ++i) {
            auto wheel_pos = kc_car_wheel_position(car_, i);
            auto wheel_rot = kc_car_wheel_rotation(car_, i);

            car_wheels_[i] = stage->new_actor_with_mesh(wheel_mesh_);
            stage->actor(car_wheels_[i])->move_to(wheel_pos);
            stage->actor(car_wheels_[i])->set_absolute_rotation(wheel_rot);
        }

        // At the end of each frame, trigger the work end frame callback
        window->signal_frame_finished().connect(std::bind(kc_world_end_frame, physics_world_));
    }

    void do_activate() {
        window->keyboard->key_pressed_connect(SDL_SCANCODE_SPACE, [&](SDL_Keysym) {
            step_mode = true;
            kc_world_step(physics_world_, 1.0 / 120.0);
        });
    }

    void do_deactivate() {

    }

    void do_unload() {
        kc_world_destroy(physics_world_);
    }

    void do_step(double dt) {
        if(window->keyboard->key_state(SDL_SCANCODE_LEFT)) {
            kc_car_turn_left(car_);
        }

        if(window->keyboard->key_state(SDL_SCANCODE_RIGHT)) {
            kc_car_turn_right(car_);
        }

        if(window->keyboard->key_state(SDL_SCANCODE_UP)) {
            kc_car_accelerate(car_);
        }

        if(window->keyboard->key_state(SDL_SCANCODE_DOWN)) {
            kc_car_brake(car_);
        }

        if(!step_mode) {
            kc_world_step(physics_world_, dt);
        }

        kmVec3 position = kc_car_position(car_);
        kmQuaternion rotation = kc_car_rotation(car_);

        auto stage = window->stage(stage_);
        auto actor = stage->actor(car_actor_);
        actor->move_to(position);
        actor->set_absolute_rotation(rotation);

        for(uint8_t i = 0; i < 4; ++i) {
            auto wheel_pos = kc_car_wheel_position(car_, i);
            auto wheel_rot = kc_car_wheel_rotation(car_, i);

            stage->actor(car_wheels_[i])->move_to(wheel_pos);
            stage->actor(car_wheels_[i])->set_absolute_rotation(wheel_rot);            
/*
            stage->debug().draw_ray(
                wheel_pos,
                kglt::Vec3(kc_car_wheel_right(car_, i)),
                kglt::Colour::LIGHT_BLUE,
                0.0
            );

            stage->debug().draw_ray(
                wheel_pos,
                kglt::Vec3(kc_car_wheel_forward(car_, i)),
                kglt::Colour::LIGHT_GREEN,
                0.0
            );*/
        }
    }

private:
    kglt::StageID stage_;
    kglt::CameraID camera_;
    kglt::PipelineID pipeline_;
    kglt::MeshID world_;

    kglt::MeshID car_mesh_;
    kglt::ActorID car_actor_;

    kglt::MeshID wheel_mesh_;
    kglt::ActorID car_wheels_[4];

    WorldID physics_world_ = 0;
    CarID car_ = 0;
    std::shared_ptr<Renderer> renderer_;
};

class Game : public kglt::Application {
public:
    bool do_init() {
        register_screen("/", kglt::screen_factory<GameScreen>());
        return true;
    }
};

int main(int argc, char* argv[]) {
    Game game;
    return game.run();
}
