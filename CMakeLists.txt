# A really simple racing car physics engine with cartoon-y style
# physics. Uses half-ellipsoids + 4 rays per vehicle. Brute force
# triangle collisions. Nothing fancy!

# C++ with a C API for language portability
CMAKE_MINIMUM_REQUIRED(VERSION 2.8)
PROJECT(kazcar)

# See http://github.com/kazade/kazmath
FIND_PACKAGE(KAZMATH REQUIRED)
FIND_PACKAGE(KAZTEST REQUIRED)

INCLUDE_DIRECTORIES(
    ${KAZMATH_INCLUDE_DIRS}
)

LINK_LIBRARIES(
    ${KAZMATH_LIBRARIES}
)

FILE(GLOB_RECURSE SOURCES FOLLOW_SYMLINKS kazcar/*.cpp kazcar/*c)

SET(
    CMAKE_CXX_FLAGS "-std=c++14 -pthread"
)

ADD_DEFINITIONS("-Wall -g -O0 -fPIC")

ADD_LIBRARY(kazcar SHARED ${SOURCES})
SET_TARGET_PROPERTIES(
    kazcar
    PROPERTIES
    VERSION 0.0.1
    SOVERSION 1
)

ADD_SUBDIRECTORY(tests)
ADD_SUBDIRECTORY(playground)
