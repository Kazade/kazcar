#ifndef AI_CONTROLLER_H
#define AI_CONTROLLER_H

#include "kazcar.h"

class World;

class AIController {
public:
    friend AIControllerID create_ai_controller(WorldID world_id, RouteID route_id, AILevel level);

    AIController(AIControllerID id, World* world);

    AIControllerID id() const { return id_; }
    void start();
    void pause();
    void resume();

    void set_car(CarID car_id);
private:
    AIControllerID id_;
    World* world_ = nullptr;
    RouteID route_id_;
    AILevel level_;
    CarID target_car_ = 0;
};

#endif // AI_CONTROLLER_H

