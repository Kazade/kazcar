#ifndef RIGID_BODY_H
#define RIGID_BODY_H

#include <kazmath/kazmath.h>

enum RigidBodyShape {
    RIGID_BODY_SHAPE_SPHERE = 0,
    RIGID_BODY_SHAPE_BOX
};

class Body {
public:
    Body();

    void apply_force(const kmVec3& force);
    void apply_force_at(const kmVec3& force, const kmVec3& position);
    void apply_local_force(const kmVec3& force);

    void apply_impulse(const kmVec3& impulse);
    void apply_impulse_at(const kmVec3& force, const kmVec3& position);
    void apply_local_impulse(const kmVec3& impulse);

    void apply_torque(const kmVec3& force);
    void apply_angular_impulse(const kmVec3& force);

    void set_angular_damping(float v) { angular_damping_ = v; }
    void set_linear_damping(float v) { linear_damping_ = v; }

    void set_linear_velocity(const kmVec3& linear_velocity) { linear_velocity_ = linear_velocity; }
    const kmVec3& linear_velocity() const { return linear_velocity_; }
    const kmVec3& angular_velocity() const { return angular_velocity_; }

    void set_shape_box(kmVec3 dimensions);
    void set_shape_sphere(float diameter);

    void set_mass(float value);
    float mass() const { return mass_; }
    float inv_mass() const { return inv_mass_; }

    void update(double dt);

    void set_position(const kmVec3& position) { position_ = position; }
    void set_rotation(const kmQuaternion& rotation) { rotation_ = rotation; }

    const kmQuaternion& rotation() const { return rotation_; }
    const kmVec3& position() const { return position_; }

    kmVec3 linear_velocity_at(const kmVec3& point);

    kmVec3 up() const { kmVec3 ret; kmQuaternionGetUpVec3(&ret, &rotation()); return ret; }
    kmVec3 right() const { kmVec3 ret; kmQuaternionGetRightVec3(&ret, &rotation()); return ret; }

    kmVec3 center_of_mass() const { return position_; }
private:
    kmVec3 position_;
    kmQuaternion rotation_;

    kmVec3 force_;
    kmVec3 linear_velocity_;

    kmVec3 torque_;
    kmVec3 angular_velocity_;

    float linear_damping_ = 0.0f;
    float angular_damping_ = 0.0f;

    // Mass properties
    RigidBodyShape shape_;
    kmVec3 shape_dimensions_;

    float mass_ = 1.0;
    float inv_mass_ = 1.0;

    kmMat3 inertia_tensor_;
    kmMat3 inverse_inertia_tensor_;
    kmMat3 inverse_inertia_tensor_WS_; // Inertia tensor transformed to world space

    kmVec3 center_of_mass_;

    void calculate_inertia();
    void recalc_world_inertia();
};

#endif // RIGID_BODY_H

