#pragma once

#include <kazmath/kazmath.h>
#include <vector>

struct CollisionInfo {
    kmVec3 normal;
    kmVec3 intersection;
    float intersection_depth;
};

struct LineSegment {
    kmVec3 p1;
    kmVec3 p2;
};

struct Sphere {
    float radius;
    kmVec3 position;
};

struct Ellipsoid {
    kmVec3 radii;
    kmVec3 position;
};

struct Triangle {
    kmVec3 vertices[3];
    kmVec3 normal;

    void recalc_normal() {
        kmVec3 v1, v2;
        kmVec3Subtract(&v1, &vertices[1], &vertices[0]);
        kmVec3Subtract(&v2, &vertices[2], &vertices[0]);

        kmVec3Cross(&normal, &v1, &v2);
        kmVec3Normalize(&normal, &normal);
    }
};

struct OBB {
    kmVec3 dimensions;
    kmVec3 position;
    kmQuaternion rotation;

    std::vector<kmVec3> calc_points() const {
        std::vector<kmVec3> points;

        float hw = dimensions.x / 2.0;
        float hh = dimensions.y / 2.0;
        float hd = dimensions.z / 2.0;

        points.push_back({-hw, -hh, -hd});
        points.push_back({ hw, -hh, -hd});
        points.push_back({ hw,  hh, -hd});
        points.push_back({-hw,  hh, -hd});

        points.push_back({-hw, -hh, hd});
        points.push_back({ hw, -hh, hd});
        points.push_back({ hw,  hh, hd});
        points.push_back({-hw,  hh, hd});

        for(auto& p: points) {
            kmQuaternionMultiplyVec3(&p, &rotation, &p);
            kmVec3Add(&p, &p, &position);
        }
        return points;
    }
};

struct Dome {
    float radius;
    kmVec3 position;
    kmVec3 up;
};

bool collide_triangle_and_ellipsoid(
    const Triangle& triangle,
    const Ellipsoid& ellipsoid,
    std::vector<CollisionInfo>& collisions
);

bool collide_triangle_and_sphere(
    const Triangle& triangle,
    const Sphere& sphere,
    std::vector<CollisionInfo>& collisions
);

bool collide_triangle_and_dome(
    const Triangle& triangle,
    const Dome& dome,
    std::vector<CollisionInfo>& collisions
);

bool collide_triangle_and_ray(
    const Triangle& triangle,
    const kmRay3& ray,
    std::vector<CollisionInfo>& collisions
);

bool intersect_sphere_line_segment(const Sphere& sphere, const LineSegment& line, std::vector<CollisionInfo>& collisions);

bool collide_triangle_and_obb(
    const Triangle& triangle,
    const OBB& obb,
    std::vector<CollisionInfo>& collisions
);

bool collide_obb_and_obb(
    const OBB& obb1,
    const OBB& obb2,
    std::vector<CollisionInfo>& collisions
);
