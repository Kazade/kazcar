#include <algorithm>
#include "ray_caster.h"


bool RayCaster::cast_ray(const kmVec3& start, const kmVec3& direction, std::vector<RayCollision>& collisions) const {
    kmRay3 ray;
    ray.start = start;
    ray.dir = direction;

    for(const Triangle& tri: triangles_) {
        RayCollision info;
        if(kmRay3IntersectTriangle(&ray, &tri.vertices[0], &tri.vertices[1], &tri.vertices[2], &info.intersection, &info.normal, &info.distance)) {
            collisions.push_back(info);
        }
    }

    std::sort(collisions.begin(), collisions.end(), [](auto& lhs, auto& rhs) -> bool { return lhs.distance < rhs.distance; });
    return !collisions.empty();
}

void RayCaster::add_triangle(const Triangle& triangle) {
    // FIXME: Spacial hash
    auto t = triangle;
    t.recalc_normal();
    triangles_.push_back(t);
}

void RayCaster::add_triangle(const kmVec3& v1, const kmVec3& v2, const kmVec3& v3) {
    Triangle triangle;
    triangle.vertices[0] = v1;
    triangle.vertices[1] = v2;
    triangle.vertices[2] = v3;
    add_triangle(triangle);
}
