#ifndef RAY_CASTER_H
#define RAY_CASTER_H

#include "collision.h"

struct RayCollision {
    float distance;
    kmVec3 intersection;
    kmVec3 normal;
};

class RayCaster {
public:
    bool cast_ray(const kmVec3& start, const kmVec3& direction, std::vector<RayCollision>& collisions) const;
    void add_triangle(const Triangle& t);
    void add_triangle(const kmVec3& v1, const kmVec3& v2, const kmVec3& v3);

    const std::vector<Triangle>& triangles() const { return triangles_; }
private:
    std::vector<Triangle> triangles_;
};

#endif // RAY_CASTER_H

