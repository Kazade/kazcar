#include <vector>
#include <iostream>
#include <memory>
#include <unordered_map>
#include <limits>
#include <fstream>

#include "kazcar_private.h"

#include "car.h"
#include "route.h"
#include "ai_controller.h"

const bool DEBUG_LOGGING_ENABLED = true;

typedef std::unordered_map<WorldID, std::shared_ptr<World>> WorldList;
typedef std::unordered_map<CarID, std::shared_ptr<Car>> CarList;
typedef std::unordered_map<RouteID, std::shared_ptr<Route>> RouteList;
typedef std::unordered_map<AIControllerID, std::shared_ptr<AIController>> AIControllerList;

static std::shared_ptr<WorldList> worlds;
static std::shared_ptr<CarList> cars;
static std::shared_ptr<RouteList> routes;
static std::shared_ptr<AIControllerList> ai_controllers;

const std::string LOG_FILE = "/tmp/kazcar.log";

void write_log(const std::string& str) {
    if(!DEBUG_LOGGING_ENABLED) return;

    static std::shared_ptr<std::ofstream> fileout;
    if(!fileout) {
        fileout = std::make_shared<std::ofstream>();
        fileout->open(LOG_FILE);
    }

    *fileout << str;
    fileout->flush();
}



void init_state() {
    if(!worlds) {
        worlds = std::make_shared<WorldList>();
    }

    if(!cars) {
        cars = std::make_shared<CarList>();
    }

    if(!routes) {
        routes = std::make_shared<RouteList>();
    }
}

WorldID create_world() {
    static uint32_t world_counter = 0;

    write_log("Created world");

    init_state();

    auto world = std::make_shared<World>(++world_counter);
    (*worlds)[world_counter] = world;
    return world->id();
}

World* find_world(WorldID world) {
    init_state();

    return worlds->at(world).get();
}

void destroy_world(WorldID world) {
    init_state();

    worlds->erase(world);
}

template<typename IDType, typename ObjType, typename ObjListPtr>
IDType world_factory(WorldID world_id, ObjListPtr& list, std::function<void (World*, uint32_t)> func) {
    static uint32_t counter = 0;
    init_state();

    auto world = find_world(world_id);
    auto obj = std::make_shared<ObjType>(++counter, world);
    (*list)[counter] = obj;
    func(world, obj->id());
    return obj->id();
}


CarID create_car(WorldID world_id) {
    return world_factory<CarID, Car>(world_id, cars, &World::add_car);
}

Car* find_car(CarID car) {
    init_state();
    return cars->at(car).get();
}

void destroy_car(CarID car) {
    init_state();

    cars->erase(car);
}

RouteID create_route(WorldID world_id) {
    return world_factory<RouteID, Route>(world_id, routes, &World::add_route);
}

Route* find_route(RouteID route_id) {
    init_state();
    return routes->at(route_id).get();
}

void destroy_route(RouteID route_id) {
    init_state();
    routes->erase(route_id);
}

AIControllerID create_ai_controller(WorldID world_id, RouteID route_id, AILevel level) {
    // This is a friend of the AIController

    auto id = world_factory<AIControllerID, AIController>(world_id, ai_controllers, &World::add_ai_controller);

    find_ai_controller(id)->route_id_ = route_id;
    find_ai_controller(id)->level_ = level;

    return id;
}

AIController* find_ai_controller(AIControllerID controller_id) {
    init_state();
    return ai_controllers->at(controller_id).get();
}

void destroy_ai_controller(AIControllerID controller_id) {
    init_state();
    ai_controllers->erase(controller_id);
}

// ================= WORLD =====================

World::World(WorldID id):
    id_(id) {

}

void World::end_frame() {
    // At the end of each frame, reset the car inputs
    for(auto car_id: cars_) {
        auto car = find_car(car_id);
        car->reset_input();
    }
}

void World::step(double dt) {
    const kmVec3 grv = { 0, -9.81, 0 };

    write_log("===== FRAME =====\n");

    for(auto car_id: cars_) {
        auto car = find_car(car_id);

        kmVec3 gravity;
        kmVec3Scale(&gravity, &grv, car->body()->mass());

        // ===== APPLY STEERING / ACCELERATION ==
        car->body()->apply_force(gravity);
        car->update(caster_, dt);

    }

    write_log("===== END FRAME =====\n");

    // Trigger the render callback after each step
    if(cb_) {
        std::vector<kmVec3> vertices;
        std::vector<uint32_t> indices;
        std::vector<kmVec3> normals;

        uint32_t index_counter = 0;

        // Render the world vertices
        // FIXME: Cache this.. it doesn't change unless we add_triangle

        for(auto& tri: caster_.triangles()) {
            vertices.push_back(tri.vertices[0]);
            indices.push_back(index_counter++);

            vertices.push_back(tri.vertices[1]);
            indices.push_back(index_counter++);

            vertices.push_back(tri.vertices[2]);
            indices.push_back(index_counter++);

            // Same normal for all vertices
            for(uint32_t i = 0; i < 3; ++i) {
                normals.push_back(tri.normal);
            }
        }

        cb_(&vertices[0], &normals[0], vertices.size(), &indices[0], indices.size());
    }
}

void World::add_triangle(const kmVec3 *v1, const kmVec3 *v2, const kmVec3 *v3) {
    caster_.add_triangle(*v1, *v2, *v3);
}

void World::add_car(CarID car) {
    cars_.push_back(car);
}

void World::add_route(RouteID route_id) {
    routes_.push_back(route_id);
}

void World::add_ai_controller(AIControllerID ai_id) {
    ai_controllers_.push_back(ai_id);
}

void World::set_gravity(const kmVec3 *grv) {

}


