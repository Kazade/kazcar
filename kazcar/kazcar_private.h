#pragma once

#include <list>
#include <array>
#include <functional>
#include <sstream>

#include "kazcar.h"
#include "collision.h"
#include "ray_caster.h"

class Car;
class Route;
class AIController;

typedef std::function<void (const kmVec3*, const kmVec3*, uint32_t, const uint32_t*, uint32_t)> RenderCallback;

class World {
public:
    World(WorldID id);

    void set_gravity(const kmVec3* grv);
    void step(double dt);
    void end_frame();
    void add_triangle(const kmVec3* v1, const kmVec3* v2, const kmVec3* v3);
    void add_car(CarID car);
    void add_route(RouteID route_id);
    void add_ai_controller(AIControllerID ai_id);
    void set_render_callback(RenderCallback cb) { cb_ = cb; }

    WorldID id() const { return id_; }

private:
    WorldID id_;
    RayCaster caster_;
    std::list<CarID> cars_;
    std::list<RouteID> routes_;
    std::list<AIControllerID> ai_controllers_;

    RenderCallback cb_;
};

WorldID create_world();
World* find_world(WorldID world);
void destroy_world(WorldID world);

CarID create_car(WorldID world);
Car* find_car(CarID car);
void destroy_car(CarID car);

RouteID create_route(WorldID world_id);
Route* find_route(RouteID route_id);
void destroy_route(RouteID route_id);

AIControllerID create_ai_controller(WorldID world_id, RouteID route_id, AILevel level);
AIController* find_ai_controller(AIControllerID controller_id);
void destroy_ai_controller(AIControllerID controller_id);

extern const bool DEBUG_LOGGING_ENABLED;
void write_log(const std::string& str);

template<typename T>
void write_log(const std::string& str, T value) {
    if(!DEBUG_LOGGING_ENABLED) return;

    std::string copy = str;

    std::stringstream ss;
    ss << value;

    auto placeholder = copy.find("{}");
    copy = copy.replace(placeholder, 2, ss.str());
    write_log(copy);
}

template<typename T, typename... Args>
void write_log(const std::string& str, T value, const Args&... args) {
    if(!DEBUG_LOGGING_ENABLED) return;

    std::string copy = str;

    std::stringstream ss;
    ss << value;

    auto placeholder = copy.find("{}");
    copy = copy.replace(placeholder, 2, ss.str());
    write_log(copy, args...);
}
