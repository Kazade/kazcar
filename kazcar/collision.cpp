#include <cstdint>
#include <vector>
#include <limits>

#include "collision.h"

bool collide_triangle_and_ellipsoid(const Triangle& triangle, const Ellipsoid& ellipsoid, std::vector<CollisionInfo>& collisions) {
    Triangle new_triangle = triangle;
    auto& dimensions = ellipsoid.radii;

    // Scale the triangle to ellipsoid space
    for(uint32_t i = 0; i < 3; ++i) {
        new_triangle.vertices[i].x /= dimensions.x;
        new_triangle.vertices[i].y /= dimensions.y;
        new_triangle.vertices[i].z /= dimensions.z;
    }

    // Unit sphere
    Sphere sphere;
    sphere.position = ellipsoid.position;
    sphere.radius = 0.5;

    bool ret = collide_triangle_and_sphere(triangle, sphere, collisions);

    //FIXME: Unscale collision info

    return ret;
}

bool check_point_in_triangle(const Triangle& triangle, const kmVec3& point) {

    auto same_side = [](const kmVec3& p1, const kmVec3 &p2, const kmVec3& a, const kmVec3& b) {
        kmVec3 edge;
        kmVec3Subtract(&edge, &b, &a);

        kmVec3 ref1, ref2;
        kmVec3Subtract(&ref1, &p1, &a);
        kmVec3Subtract(&ref2, &p2, &a);

        kmVec3 cp1, cp2;
        kmVec3Cross(&cp1, &edge, &ref1);
        kmVec3Cross(&cp2, &edge, &ref2);

        if(kmVec3Dot(&cp1, &cp2) >= 0) {
            return true;
        }

        return false;
    };

    if(same_side(point, triangle.vertices[0], triangle.vertices[1], triangle.vertices[2]) &&
       same_side(point, triangle.vertices[1], triangle.vertices[0], triangle.vertices[2]) &&
       same_side(point, triangle.vertices[2], triangle.vertices[0], triangle.vertices[1])) {
        return true;
    }

    return false;
}

kmVec3 line_segment_direction(const LineSegment& segment) {
    kmVec3 dir;
    kmVec3Subtract(&dir, &segment.p2, &segment.p1);
    return dir;
}

kmVec3 line_segment_nearest_point_to(const LineSegment& segment, const kmVec3& point) {
    auto line_dir = line_segment_direction(segment);
    kmVec3 point_dir;
    kmVec3Subtract(&point_dir, &point, &segment.p1);

    float dir_dot = kmVec3Dot(&line_dir, &line_dir);
    assert(dir_dot != 0.0);
    float t = kmVec3Dot(&line_dir, &point_dir) / dir_dot;

    if(t < 0.0) {
        return segment.p1;
    } else if(t > 1.0) {
        return segment.p2;
    }

    // Build the result from p1 + (t * dir)
    kmVec3Scale(&line_dir, &line_dir, t);
    kmVec3Add(&line_dir, &line_dir, &segment.p1);
    return line_dir;
}

bool intersect_sphere_line_segment(const Sphere& sphere, const LineSegment& line, std::vector<CollisionInfo>& collisions) {
    // http://www.lighthouse3d.com/tutorials/maths/ray-sphere-intersection/

    auto c = sphere.position;
    auto r = sphere.radius;

    std::vector<kmVec3> intersections;

    auto sphere_outside_ray = [&](const kmVec3& d, const kmVec3& vpc, const kmVec3& p) -> bool {
        kmVec3 intersection;
        auto vpc_length = kmVec3Length(&vpc);
        if(vpc_length > r) {
            // No intersection, we are further from the start of the
            // ray than the circle's radius
            return false;
        } else if(vpc_length == r) {
            //The start of the ray is the intersection point
            kmVec3Assign(&intersection, &p);
            intersections.push_back(intersection);
            return true;
        } else {
            // This block is for when we intersect with the origin of the
            // ray somehow but the projected point of the sphere position
            // is outside the ray

            // Find the point on the line nearest to the sphere center
            auto pc = line_segment_nearest_point_to(line, c);

            // Get the vector from the point on the line, to the
            // origin of the ray (essentially the direction of the
            // ray scaled to the distance of the intersection on the line)
            kmVec3 pcp;
            kmVec3Subtract(&pcp, &pc, &p);

            // Find the vector from the point on the line
            // to the sphere position
            kmVec3 tmp;
            kmVec3Subtract(&tmp, &pc, &c);
            auto tmp_length = kmVec3Length(&tmp);

            // Triangle in'it. We have the vector from the
            // sphere position to the point on the line. By
            // using the length of the circle radius we can pretend
            // that another side of the triangle is from the centre
            // of the sphere to the point where the sphere
            // crosses the line. With that, we can find the distance between
            // where the projected point is, and where the edge of the sphere
            // crosses.
            auto dist = sqrt(
                (r * r) - (tmp_length * tmp_length)
            );

            // So the final t-value is this distance, minus the distance that
            // the projected point is away from the start of the ray (remember
            // we are here because the projected point is outside the ray)
            auto t = dist - kmVec3Length(&pcp);

            // Calculate the intersection by scaling the ray direction
            // by t and adding the ray origin to it
            kmVec3Scale(&intersection, &d, t);
            kmVec3Add(&intersection, &intersection, &p);

            intersections.push_back(intersection);
            return true;
        }
    };


    auto p1 = line.p1;
    auto p2 = line.p2;

    kmVec3 d;
    kmVec3Subtract(&d, &line.p2, &line.p1);
    kmVec3Normalize(&d, &d);

    kmVec3 neg_d;
    kmVec3Scale(&neg_d, &d, -1.0);

    kmVec3 vp1c;
    kmVec3Subtract(&vp1c, &c, &p1);
    kmVec3 vp2c;
    kmVec3Subtract(&vp2c, &c, &p2);

    if(kmVec3Dot(&vp1c, &d) < 0) {
        //Sphere is behind the origin of the ray
        return sphere_outside_ray(d, vp1c, p1);
    } else if(kmVec3Dot(&vp2c, &neg_d) < 0) {
        // Sphere is beyond the end of the line segment
        return sphere_outside_ray(neg_d, vp2c, p2);
    } else {
        // Center of the sphere projects onto the ray somewhere

        // Find the point on the line nearest to the sphere center
        auto pc = line_segment_nearest_point_to(line, c);
        kmVec3 cpc;
        kmVec3Subtract(&cpc, &c, &pc);
        auto cpc_length = kmVec3Length(&cpc);
        if(cpc_length > r) {
            // No intersection, the sphere is too far from the line
            return false;
        } else {
            kmVec3 tmp;
            kmVec3Subtract(&tmp, &pc, &c);
            auto tmp_length = kmVec3Length(&tmp);
            auto dist = sqrt(
                (r * r) - (tmp_length * tmp_length)
            );

            kmVec3 pcp;
            kmVec3Subtract(&pcp, &pc, &p1);
            auto pcp_length = kmVec3Length(&pcp);
            float t;

            if(kmVec3Length(&vp1c) > r) {
                // Ray origin is outside the sphere
                t = pcp_length - dist;
            } else {
                t = pcp_length + dist;
            }

            kmVec3 intersection;
            kmVec3Scale(&intersection, &d, t);
            kmVec3Add(&intersection, &intersection, &p1);
            intersections.push_back(intersection);

            // FIXME: Add other intersection!
            return true;
        }
    }

    return false;
}

bool collide_triangle_and_sphere(const Triangle& triangle, const Sphere& sphere, std::vector<CollisionInfo>& collisions) {
    auto& verts = triangle.vertices;
    auto& position = sphere.position;

    // Step 1. Plane check

    // Construct the triangle's plane (we could do this once when the triangle is created)
    kmPlane plane;
    kmPlaneFromPoints(&plane, &triangle.vertices[0], &triangle.vertices[1], &triangle.vertices[2]);

    // Construct a ray travelling in the direction of the triangle
    kmVec3 direction;
    kmVec3Scale(&direction, &triangle.normal, -1.0);

    kmRay3 ray;
    kmRay3FromPointAndDirection(&ray, &position, &direction);

    kmVec3 intersection;
    if(kmRay3IntersectPlane(&intersection, &ray, &plane)) {
        // If we intersect the triangles plane, we need to see if the point is contained within
        // the triangle, or within a distance of 1.0 from any of the vertices or edges

        kmVec3 diff;
        kmVec3Subtract(&diff, &intersection, &position);

        if(kmVec3Length(&diff) > 0.5) {
            return false;
        }

        // Step 2. Point inside triangle
        if(check_point_in_triangle(triangle, intersection)) {
            // FIXME: Fill collision info
            return true;
        }

        // Step 3. Point check
        for(uint8_t i = 0; i < 3; ++i) {
            // First check against the triangle's vertices (fast check)
            kmVec3Subtract(&diff, &verts[i], &position);
            if(kmVec3Length(&diff) <= 0.5) {
                // We collided
                //FIXME: Fill collision info
                return true;
            }
        }

        // Step 4. Edge check
        for(uint8_t i = 0; i < 3; ++i) {
            LineSegment line;

            line.p1 = triangle.vertices[i];
            line.p2 = triangle.vertices[(i == 2) ? 0 : i + 1];

            std::vector<CollisionInfo> line_collisions;

            if(intersect_sphere_line_segment(sphere, line, line_collisions)) {
                //FIXME: Fill collision info
                return true;
            }
        }
    }

    return false;
}

bool collide_triangle_and_dome(const Triangle& triangle, const Dome& dome, std::vector<CollisionInfo>& collisions) {
    // Standard sphere <> triangle collision but ignoring collisions where the normal.dot(up) > 0
    // unless the distance < -radius in which case the normal is downward

    // FIXME: Call ellipsoid check, disregard collisions below radius in relation to dome's UP
}

bool collide_triangle_and_ray(const Triangle& triangle, const kmRay3& ray, std::vector<CollisionInfo>& collisions) {
    // Standard ray <> triangle intersection
    collisions.clear();

    kmPlane plane;
    kmVec3 intersection;

    kmPlaneFromPointAndNormal(&plane, &triangle.vertices[0], &triangle.normal);
    if(!kmRay3IntersectPlane(&intersection, &ray, &plane)) {
        return false;
    }

    if(check_point_in_triangle(triangle, intersection)) {
        CollisionInfo info;
        info.intersection = intersection;
        kmVec3Fill(&info.normal, plane.a, plane.b, plane.c);
        auto dot = kmVec3Dot(&ray.start, &info.normal);
        info.intersection_depth = dot - plane.d;
        collisions.push_back(info);
        return true;
    }

    return false;
}


void sat_test(const kmVec3& axis, const std::vector<kmVec3>& points, float& min_value, float& max_value) {
    min_value = std::numeric_limits<float>::max();
    max_value = std::numeric_limits<float>::lowest();

    for(auto& p: points) {
        auto dot_val = kmVec3Dot(&p, &axis);
        if(dot_val < min_value) min_value = dot_val;
        if(dot_val > max_value) max_value = dot_val;
    }
}

auto is_between_ordered = [](float val, float lower_bound, float upper_bound) -> bool {
    return lower_bound <= val && val <= upper_bound;
};

auto overlaps = [](float min1, float max1, float min2, float max2) -> bool {
    return is_between_ordered(min2, min1, max1) ||
           is_between_ordered(max2, min1, max1) ||
           is_between_ordered(max1, min2, max2) ||
           is_between_ordered(min1, min2, max2);
};

bool collide_triangle_and_obb(const Triangle& triangle, const OBB& obb, std::vector<CollisionInfo>& collisions) {
    /*
     *  Use separating axis theorum to detect collisions
     */

    // http://gamedev.stackexchange.com/questions/25397/obb-vs-obb-collision-detection
    // http://gamedev.stackexchange.com/questions/44500/how-many-and-which-axes-to-use-for-3d-obb-collision-with-sat

    std::vector<kmVec3> triangle_points(&triangle.vertices[0], &triangle.vertices[0] + 3);
    std::vector<kmVec3> obb_points = obb.calc_points();

    std::vector<kmVec3> axes; // The axis to check
    axes.push_back(triangle.normal);

    // Push back the 3 normals of the OBB (this relies on the ordering of OBB::recalc_points)
    kmVec3 n1, n2, n3;
    kmVec3Subtract(&n1, &obb_points[1], &obb_points[0]);
    kmVec3Subtract(&n2, &obb_points[2], &obb_points[1]);
    kmVec3Subtract(&n3, &obb_points[7], &obb_points[3]);
    kmVec3Normalize(&n1, &n1);
    kmVec3Normalize(&n2, &n2);
    kmVec3Normalize(&n3, &n3);

    // Now we need to cross product all the edge vectors to get the final axis to check
    for(uint8_t i = 0; i < 3; ++i) {
        kmVec3 new_edge;
        kmVec3Subtract(&new_edge, &triangle_points[i], &triangle_points[(i == 2) ? 0 : i + 1]);
        kmVec3Normalize(&new_edge, &new_edge);
        axes.push_back(new_edge);

        kmVec3 new_axis;
        kmVec3Cross(&new_axis, &new_edge, &n1);
        axes.push_back(new_axis);
        kmVec3Cross(&new_axis, &new_edge, &n2);
        axes.push_back(new_axis);
        kmVec3Cross(&new_axis, &new_edge, &n3);
        axes.push_back(new_axis);
    }

    for(auto& axis: axes) {
        float shape1min, shape1max, shape2min, shape2max;
        sat_test(axis, triangle_points, shape1min, shape1max);
        sat_test(axis, obb_points, shape2min, shape2max);

        if(!overlaps(shape1min, shape1max, shape2min, shape2max)) {
            // FIXME: calculate collisions
            return false;
        }
    }

    return true;
}

bool collide_obb_and_obb(const OBB& obb1, const OBB& obb2, std::vector<CollisionInfo>& collisions) {
    //Separating axis theorem - again
    auto obb1_points = obb1.calc_points();
    auto obb2_points = obb2.calc_points();

    std::vector<kmVec3> axes;

    // Push back the 3 face normals of the OBB (this relies on the ordering of OBB::recalc_points)
    kmVec3 n1, n2, n3;

    for(auto& obb_points: { obb1_points, obb2_points}) {
        kmVec3Subtract(&n1, &obb_points[1], &obb_points[0]);
        kmVec3Subtract(&n2, &obb_points[2], &obb_points[1]);
        kmVec3Subtract(&n3, &obb_points[7], &obb_points[3]);
        kmVec3Normalize(&n1, &n1);
        kmVec3Normalize(&n2, &n2);
        kmVec3Normalize(&n3, &n3);
        axes.push_back(n1);
        axes.push_back(n2);
        axes.push_back(n3);
    }

    // Cross together all the normals to get the edge normals
    for(uint8_t i = 0; i < 3; ++i) {
        for(uint8_t j = 3; j < 6; ++j) {
            kmVec3 new_axis;
            kmVec3Cross(&new_axis, &axes[i], &axes[j]);
            axes.push_back(new_axis);
        }
    }

    // Intersection test
    for(auto& axis: axes) {
        float shape1min, shape1max, shape2min, shape2max;
        sat_test(axis, obb1_points, shape1min, shape1max);
        sat_test(axis, obb2_points, shape2min, shape2max);

        if(!overlaps(shape1min, shape1max, shape2min, shape2max)) {
            // FIXME: calculate collisions
            return false;
        }
    }

    return true;

}
