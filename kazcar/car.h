#ifndef CAR_H
#define CAR_H

#include <array>
#include <memory>
#include <list>

#include "rigid_body.h"
#include "collision.h"
#include "kazcar.h"
#include "ray_caster.h"

class World;
class RayCaster;

class Car {
public:
    Car(CarID id, World* world);

    CarID id() const { return id_; }

    void accelerate();
    void brake();
    void handbrake();
    void turn_left();
    void turn_right();

    void set_position(const kmVec3& position) { body_.set_position(position); }

    void set_axel_width(float x);
    void set_axel_separation(float z);

    kmVec3 relative_wheel_position(uint8_t which) const;
    kmVec3 relative_suspension_position(uint8_t which) const;

    kmVec3 wheel_position(uint8_t which) const;
    kmVec3 suspension_position(uint8_t which) const;
    kmQuaternion wheel_rotation(uint8_t which) const;

    float max_suspension_length() const { return suspension_max_length_; }
    float tire_radius() const { return tire_radius_; }
    kmQuaternion rotation() const { return body_.rotation(); }
    float spring_constant() const { return spring_constant_; }
    float damping_constant() const { return damping_constant_; }
    float previous_wheel_compression(uint8_t wheel) { return wheels_[wheel].previous_compression; }
    void set_previous_wheel_compression(uint8_t wheel, float comp) {
        wheels_[wheel].previous_compression = comp;
    }

    kmVec3 wheel_right_axis(uint8_t which, const kmVec3& up) const;
    kmVec3 wheel_forward_axis(uint8_t which, const kmVec3& up) const;

    RayCollision* wheel_collision_data(uint8_t which) const {
        return wheels_[which].ray_cast_info.get();
    }

    Body* body() { return &body_; }
    const Body* body() const { return &body_; }

    void reset_input() {
        accelerating_ = braking_ = turning_left_ = turning_right_ = handbraking_ = false;
    }

    void apply_forces(double dt);

    float motor_force() const { return motor_force_; }
    float brake_force() const { return brake_force_; }
    float axel_width() const { return axel_width_; }
    float axel_separation() const { return axel_separation_; }
    float current_suspension_length(uint8_t wheel) const {
        auto ret = wheels_[wheel].previous_compression;
        if(fabs(ret) < kmEpsilon) {
            ret = max_suspension_length();
        }
        return ret;
    }

    void update(const RayCaster& caster, double dt);
private:
    void cast_rays(const RayCaster &caster);
    void calculate_suspension_forces();
    void apply_suspension_forces(double dt);
    void update_friction(double dt);

    enum {
        WHEEL_FL = 0,
        WHEEL_FR = 1,
        WHEEL_BL = 2,
        WHEEL_BR = 3
    };

    void recalc_wheels() {
        float haw = axel_width_ * 0.5f;
        float has = axel_separation_ * 0.5;
        wheels_[WHEEL_FL].transform = { -haw, 0, -has };
        wheels_[WHEEL_FR].transform = {  haw, 0, -has };
        wheels_[WHEEL_BL].transform = { -haw, 0,  has };
        wheels_[WHEEL_BR].transform = {  haw, 0,  has };
    }

    CarID id_;
    World* world_;

    Body body_;
    OBB obb_;

    // Configuration
    float axel_width_ = 1.8;
    float axel_separation_ = 5.0;
    float suspension_max_length_ = 0.5;
    float tire_radius_ = 0.3;
    float spring_constant_ = 10.0;
    float damping_constant_ = 35.0;
    float max_suspension_force = 6000.0f;

    float handbrake_force_ = 10.0;
    float acceleration_ = 75.0;
    float deceleration_ = 50.0;
    float turn_speed_ = 25.0; // Degrees per second
    float turn_limit_ = 25.0;

    struct Wheel {
        std::unique_ptr<RayCollision> ray_cast_info;
        float suspension_force;

        kmVec3 transform; // Relative to the car centre
        float previous_compression = 0;
        float rotation = 0; // Rotation around the car's Y-axis (in degrees)
    };

    std::array<Wheel, 4> wheels_;

    bool accelerating_ = false;
    bool braking_ = false;
    bool turning_left_ = false;
    bool turning_right_ = false;
    bool handbraking_ = false;

    float motor_force_ = 0.0f;
    float brake_force_ = 0.0f;
};


#endif // CAR_H

