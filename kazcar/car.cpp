#include <limits>
#include <algorithm>

#include "car.h"
#include "kazcar_private.h"

// ================= CAR ======================

Car::Car(CarID id, World *world):
    id_(id),
    world_(world) {

    recalc_wheels();
    body()->set_mass(5.0);
}

void Car::set_axel_width(float x) {
    assert(x > 0);
    axel_width_ = x;
    recalc_wheels();

    body()->set_shape_box({
        axel_width_,
        0,
        axel_separation_ * 1.1f
    });
}

void Car::set_axel_separation(float z) {
    assert(z > 0);
    axel_separation_ = z;
    recalc_wheels();

    // We set the car shape to be 10% longer than the
    // wheel separation
    body()->set_shape_box({
        axel_width_,
        0,
        axel_separation_ * 1.1f
    });
}


kmVec3 Car::relative_suspension_position(uint8_t which) const {
    auto rot = rotation();
    kmVec3 result;
    kmQuaternionMultiplyVec3(&result, &rot, &wheels_[which].transform);
    return result;
}

kmVec3 Car::suspension_position(uint8_t which) const {
    kmVec3 result = relative_suspension_position(which);
    kmVec3Add(&result, &result, &body()->position());
    return result;
}

kmVec3 Car::relative_wheel_position(uint8_t which) const {
    auto rot = rotation();

    kmVec3 result;

    kmVec3 downward_offset = { 0, -this->current_suspension_length(which), 0 };
    kmVec3Add(&downward_offset, &downward_offset, &wheels_[which].transform);
    kmQuaternionMultiplyVec3(&result, &rot, &downward_offset);

    assert(!isnan(result.x));
    assert(!isnan(result.y));
    assert(!isnan(result.z));
    return result;
}

kmVec3 Car::wheel_position(uint8_t which) const {
    kmVec3 result = relative_wheel_position(which);
    kmVec3Add(&result, &result, &body()->position());
    return result;
}

kmQuaternion Car::wheel_rotation(uint8_t which) const {
    kmQuaternion result;
    kmVec3 up = body()->up();
    kmQuaternionRotationAxisAngle(&result, &up, -kmDegreesToRadians(wheels_[which].rotation));
    kmQuaternionMultiply(&result, &body()->rotation(), &result);
    return result;
}

kmVec3 Car::wheel_right_axis(uint8_t which, const kmVec3& up) const {
    kmVec3 final = KM_VEC3_POS_X;
    kmQuaternion rot = wheel_rotation(which);

    kmQuaternionMultiplyVec3(&final, &rot, &final);
    write_log("Right axis for wheel {} is {}, {}, {}\n", +which, final.x, final.y, final.z);

    // Now project onto the up normal...
    float proj = kmVec3Dot(&final, &up);
    kmVec3 scaled_up;
    kmVec3Scale(&scaled_up, &up, proj);
    kmVec3Subtract(&final, &final, &scaled_up);
    kmVec3Normalize(&final, &final);
    return final;
}

kmVec3 Car::wheel_forward_axis(uint8_t which, const kmVec3& up) const {
    kmVec3 final = KM_VEC3_NEG_Z;
    kmQuaternion rot = wheel_rotation(which);

    kmQuaternionMultiplyVec3(&final, &rot, &final);
    write_log("Forward axis for wheel {} is {}, {}, {}\n", +which, final.x, final.y, final.z);

    // Now project onto the up normal...
    float proj = kmVec3Dot(&final, &up);
    kmVec3 scaled_up;
    kmVec3Scale(&scaled_up, &up, proj);
    kmVec3Subtract(&final, &final, &scaled_up);
    kmVec3Normalize(&final, &final);
    return final;
}

void Car::accelerate() {
    accelerating_ = true;
}

void Car::brake() {
    braking_ = true;
}

void Car::handbrake() {
    handbraking_ = true;
}

void Car::turn_left() {
    turning_left_ = true;
}

void Car::turn_right() {
    turning_right_ = true;
}

void Car::apply_forces(double dt) {
    brake_force_ = motor_force_ = 0.0;

    if(accelerating_) {
        motor_force_ += acceleration_ * dt;
    }

    if(braking_) {
        motor_force_ -= deceleration_ * dt;
    }

    if(handbraking_) {
        brake_force_ = handbrake_force_;
    }

    // Auto re-center the front wheels when we aren't turning
    auto sign = [](auto& value) -> int { return (value < 0) ? -1 : 1; };
    if(!turning_left_ && !turning_right_) {
        wheels_[WHEEL_FL].rotation -= sign(wheels_[WHEEL_FL].rotation) * turn_speed_ * dt;
        wheels_[WHEEL_FR].rotation -= sign(wheels_[WHEEL_FR].rotation) * turn_speed_ * dt;
    }

    if(turning_left_) {
        wheels_[WHEEL_FL].rotation -= turn_speed_ * dt;
        wheels_[WHEEL_FR].rotation -= turn_speed_ * dt;

        if(wheels_[WHEEL_FL].rotation < -turn_limit_) {
            wheels_[WHEEL_FL].rotation = -turn_limit_;
        }

        if(wheels_[WHEEL_FR].rotation < -turn_limit_) {
            wheels_[WHEEL_FR].rotation = -turn_limit_;
        }
    }

    if(turning_right_) {
        wheels_[WHEEL_FL].rotation += turn_speed_ * dt;
        wheels_[WHEEL_FR].rotation += turn_speed_ * dt;

        if(wheels_[WHEEL_FL].rotation > turn_limit_) {
            wheels_[WHEEL_FL].rotation = turn_limit_;
        }

        if(wheels_[WHEEL_FR].rotation > turn_limit_) {
            wheels_[WHEEL_FR].rotation = turn_limit_;
        }
    }
    recalc_wheels();
}

void Car::cast_rays(const RayCaster& caster) {
    float ray_length = max_suspension_length() + tire_radius();
    auto rot = rotation();

    for(uint8_t i = 0; i < 4; ++i) {
        wheels_[i].ray_cast_info.reset(); // Wipe out the info from last frame

        // sus_pos is the position where the suspension attaches
        // to the body of the car, not where the center of the wheel is
        kmVec3 ray_start = suspension_position(i);

        // Calculate a downward ray (relative to the car) of the length of the suspension
        // plus the tire radius
        kmVec3 ray_dir = { 0, -1, 0 };
        kmQuaternionMultiplyVec3(&ray_dir, &rot, &ray_dir);
        kmVec3Normalize(&ray_dir, &ray_dir);
        kmVec3Scale(&ray_dir, &ray_dir, ray_length);

        std::vector<RayCollision> collisions;
        if(caster.cast_ray(ray_start, ray_dir, collisions)) {
            // If we collided with something, then store the nearest collision info
            wheels_[i].ray_cast_info = std::make_unique<RayCollision>(collisions[0]);
        }
    }
}

void Car::calculate_suspension_forces() {
    for(uint8_t i = 0; i < 4; ++i) {
        Wheel* wheel = &wheels_[i];

        // If the wheel didn't collide, then there is no suspension force applied
        if(!wheel->ray_cast_info) {
            wheel->suspension_force = 0;
            set_previous_wheel_compression(i, 0);
            continue;
        }

        float comp = wheel->ray_cast_info->distance - tire_radius();
        float sus_minus_comp = (max_suspension_length() - comp);
        float prev_minus_comp = (previous_wheel_compression(i) - comp);

        if(sus_minus_comp < 0) sus_minus_comp = 0;
        if(prev_minus_comp < 0) prev_minus_comp = 0;

        wheel->suspension_force = (
            spring_constant() * sus_minus_comp +
            damping_constant() * prev_minus_comp
        );

        wheel->suspension_force *= body()->mass();

        if(wheel->suspension_force < 0) {
            wheel->suspension_force = 0;
        }

        set_previous_wheel_compression(i, comp);
    }
}


void Car::apply_suspension_forces(double dt) {
    for(uint8_t i = 0; i < 4; ++i) {
        Wheel* wheel = &wheels_[i];

        if(!wheel->ray_cast_info) continue;

        float suspension_force = wheel->suspension_force;

        if(suspension_force > max_suspension_force) {
            suspension_force = max_suspension_force;
        }

        kmVec3 impulse;
        kmVec3Scale(&impulse, &wheel->ray_cast_info->normal, suspension_force * dt);
        write_log("Applying suspension force to wheel {} - {}, {}, {}\n", +i, impulse.x, impulse.y, impulse.z);
        body()->apply_impulse_at(impulse, wheel->ray_cast_info->intersection);
    }
}

void Car::update_friction(double dt) {   
    for(uint8_t i = 0; i < 4; ++i) {
        Wheel* wheel = &wheels_[i];

        if(!wheel->ray_cast_info) continue;

        kmVec3 axel = wheel_right_axis(i, wheel->ray_cast_info->normal);
        kmVec3Normalize(&axel, &axel);

        auto vel = body()->linear_velocity_at(wheel->ray_cast_info->intersection);
        float rel_vel = kmVec3Dot(&axel, &vel);

        write_log("Wheel: {}, Local vel: {},{},{}, Core vel: {}, {}, {}\n", +i, vel.x, vel.y, vel.z, body()->linear_velocity().x, body()->linear_velocity().y, body()->linear_velocity().z);
        kmVec3 side_force;
        kmVec3Scale(&side_force, &axel, -rel_vel * body()->mass() * dt);
        body()->apply_impulse_at(side_force, wheel->ray_cast_info->intersection);
        write_log("Applying side force: {}, {}, {}\n", side_force.x, side_force.y, side_force.z);

        auto power = motor_force();

        // Only apply power to the front wheels, and only if there is some to apply
        if(!kmAlmostEqual(power, 0.0) && (i == WHEEL_FL || i == WHEEL_FR)) {
            kmVec3 forward = wheel_forward_axis(i, wheel->ray_cast_info->normal);
            kmVec3 forward_force;
            kmVec3Normalize(&forward_force, &forward);
            kmVec3Scale(&forward_force, &forward_force, motor_force() * body()->mass() * dt);
            body()->apply_impulse_at(forward_force, wheel->ray_cast_info->intersection);
            write_log("Applying forward force: {}, {}, {}\n", forward_force.x, forward_force.y, forward_force.z);
        }
    }
}

void Car::update(const RayCaster &caster, double dt) {
    apply_forces(dt);

    cast_rays(caster);
    calculate_suspension_forces();
    apply_suspension_forces(dt);
    update_friction(dt);


/*
    // http://hpwizard.com/tire-friction-coefficient.html
    const float friction_coefficient = 1.35;
    const float rolling_resistence = 0.020;





            // Apply the suspension force on the body at the point of intersection
            kmVec3 force;
            kmVec3Normalize(&force, &closest_normal);
            kmVec3Scale(&force, &force, suspension_force);

            write_log("Applying suspension force: {}, {}, {}\n", force.x, force.y, force.z);

            body()->apply_impulse_at(
                force,
                closest_point
            );

            // Now calculate the side force. Get the linear velocity
            auto vel = body()->linear_velocity_at(rays[i].start);

            write_log("Current velocity: {}, {}, {}\n", vel.x, vel.y, vel.z);

            auto side_force = wheel_right_axis(i, closest_normal);

            write_log("Wheel right-axis: {}, {}, {}\n", side_force.x, side_force.y, side_force.z);

            // Find the length of the vector necessary to nullify the horizontal movement
            auto dot = kmVec3Dot(&vel, &side_force);
            write_log("Dot between vel and right: {}\n", dot);

            // Scale the right vector to that length
            kmVec3Normalize(&side_force, &side_force);
            kmVec3Scale(&side_force, &side_force, -dot);

            write_log("Applying (pre-capped) side force {}, {}, {}\n", side_force.x, side_force.y, side_force.z);

            // Now calculate the forward force
            kmVec3 forward_force = wheel_forward_axis(i, closest_normal);
            write_log("Motor force: {}\n", motor_force());
            kmVec3Normalize(&forward_force, &forward_force);
            kmVec3Scale(&forward_force, &forward_force, motor_force());

            // Scale the suspension force by the friction coefficient to get the
            // max_friction_force
            kmVec3 max_friction_force;
            kmVec3Scale(&max_friction_force, &force, friction_coefficient);

            kmVec3 total_force;
            kmVec3Add(&total_force, &side_force, &forward_force);

            if(kmVec3LengthSq(&total_force) > kmVec3LengthSq(&max_friction_force)) {
                kmVec3Normalize(&total_force, &total_force);
                kmVec3Scale(&total_force, &total_force, kmVec3Length(&max_friction_force));
            }

            write_log("Applying forward force: {}, {}, {}\n", forward_force.x, forward_force.y, forward_force.z);
            body()->apply_impulse_at(total_force, closest_point);
        }

        // Make sure we store the compression value for the next iteration
        set_previous_wheel_compression(i, comp);
    }
*/

    // Perform the update applying all forces
    body()->update(dt);
}
