#pragma once

#include <stdint.h>
#include <kazmath/kazmath.h>

typedef uint32_t WorldID;
typedef uint32_t CarID;
typedef uint32_t RouteID;
typedef uint32_t BoidID;
typedef uint32_t AIControllerID;

typedef void (*DebugRenderCallback) (
    const kmVec3* vertices,
    const kmVec3* normals,
    uint32_t num_vertices,
    const uint32_t* indices,
    uint32_t num_indices,
    void* user_data
);

WorldID kc_world_create();
void kc_world_set_gravity(WorldID world, const kmVec3* gravity);
void kc_world_step(WorldID world, double dt);
void kc_world_end_frame(WorldID world);
void kc_world_add_triangle(WorldID world, const kmVec3* v1, const kmVec3* v2, const kmVec3* v3);
void kc_world_add_triangle_v(WorldID world, const kmVec3* verts);
void kc_world_set_render_callback(WorldID world_id, DebugRenderCallback callback, void* user_data);
void kc_world_destroy(WorldID world);

CarID kc_car_create(WorldID world_id);
void kc_car_set_position(CarID car, const kmVec3* position);
void kc_car_set_rotation(CarID car_id, const kmQuaternion* rotation);
void kc_car_set_dimensions(CarID car_id);

void kc_car_turn_left(CarID car_id);
void kc_car_turn_right(CarID car_id);
void kc_car_accelerate(CarID car_id);
void kc_car_brake(CarID car_id);
void kc_car_handbrake(CarID car_id);

kmVec3 kc_car_position(CarID car_id);
kmQuaternion kc_car_rotation(CarID car_id);

kmVec3 kc_car_wheel_position(CarID car_id, uint8_t wheel);
kmQuaternion kc_car_wheel_rotation(CarID car_id, uint8_t wheel);
kmVec3 kc_car_wheel_right(CarID car_id, uint8_t wheel);
kmVec3 kc_car_wheel_forward(CarID car_id, uint8_t wheel);

bool kc_car_wheel_collision_normal(CarID car_id, uint8_t wheel, kmVec3* result);

float kc_car_axel_width(CarID car_id);
float kc_car_axel_separation(CarID car_id);
float kc_car_max_suspension_length(CarID car_id);
float kc_car_current_suspension_length(CarID car_id, uint8_t wheel);
float kc_car_wheel_radius(CarID car_id);

void kc_car_destroy(CarID car);


RouteID kc_route_create(WorldID world_id);
void kc_route_push_waypoint(RouteID route_id, const kmVec3* location, const float radius);
void kc_route_set_iterations(RouteID route_id, uint32_t iterations);
uint32_t kc_route_iteration_count(RouteID route_id);
uint32_t kc_route_waypoint_count(RouteID route_id);
void kc_route_destroy(RouteID route_id);

enum AILevel {
    AI_LEVEL_POOR,
    AI_LEVEL_AVERAGE,
    AI_LEVEL_GOOD,
    AI_LEVEL_SKILLED,
    AI_LEVEL_DEADLY
};

AIControllerID kc_ai_controller_create(WorldID world_id, RouteID route, AILevel level);
void kc_ai_controller_set_car(AIControllerID boid_id, CarID car_id);
void kc_ai_controller_start(AIControllerID boid_id);
void kc_ai_controller_pause(AIControllerID boid_id);
void kc_ai_controller_resume(AIControllerID boid_id);
void kc_ai_controller_reset(AIControllerID boid_id);
void kc_ai_controller_update(AIControllerID boid_id, double dt);
void kc_ai_controller_destroy(AIControllerID boid_id);

