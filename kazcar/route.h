#ifndef ROUTE_H
#define ROUTE_H

#include <kazmath/kazmath.h>
#include <vector>
#include <cstdint>

#include "kazcar.h"

class World;

struct Waypoint {
    kmVec3 position;
    float radius;
};

class Route {
public:
    Route(RouteID id, World* world);
    void push_waypoint(const kmVec3& position, float radius);
    uint32_t waypoint_count() const;
    uint32_t iterations() const { return iterations_; }
    void set_iterations(const uint32_t i) { iterations_ = i; }

    RouteID id() const { return id_; }
private:
    RouteID id_;
    World* world_ = nullptr;
    uint32_t iterations_ = 1;
    std::vector<Waypoint> waypoints_;
};

#endif // ROUTE_H

