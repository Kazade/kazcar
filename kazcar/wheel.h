#ifndef WHEEL_H
#define WHEEL_H

#include <kazmath/kazmath.h>

struct Wheel {
    kmQuaternion rotation_ws;
    kmVec3 position_ws;
    kmVec3 ray_direction;

    float rotation; //Local Y-axis rotation
    float suspension_rest_length;
    float max_suspension_travel;
    float radius;
    float suspension_stiffness;
    float damping_compression;
    float damping_relaxation;
};

#endif // WHEEL_H

