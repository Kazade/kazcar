#include <algorithm>
#include <stdexcept>

#include "rigid_body.h"

// ================ BODY =====================

Body::Body() {
    set_shape_box({1.0, 1.0, 1.0});
    set_mass(1.0);

    kmVec3Fill(&position_, 0, 0, 0);
    kmQuaternionFill(&rotation_, 0, 0, 0, 1);
    kmVec3Fill(&center_of_mass_, 0, 0, 0);
}

void Body::apply_force(const kmVec3& force) {
    kmVec3Add(&force_, &force_, &force);
}

void Body::apply_force_at(const kmVec3& force, const kmVec3& position) {
    kmVec3 com = center_of_mass();

    kmVec3 center_dir;
    kmVec3Subtract(&center_dir, &position, &com);

    kmVec3 torque;
    kmVec3Cross(&torque, &center_dir, &force);

    apply_force(force);
    apply_torque(torque);
}

void Body::apply_impulse_at(const kmVec3& impulse, const kmVec3& position) {
    if(kmAlmostEqual(inv_mass_, 0.0)) {
        return;
    }

    kmVec3 com = center_of_mass();

    kmVec3 rel_pos;
    kmVec3Subtract(&rel_pos, &position, &com);

    kmVec3 torque;
    kmVec3Cross(&torque, &rel_pos, &impulse);

    apply_impulse(impulse);
    apply_angular_impulse(torque);
}

void Body::apply_local_force(const kmVec3& force) {
    kmVec3 rotated;
    kmQuaternionMultiplyVec3(&rotated, &rotation_, &force);
    apply_force(rotated);
}

void Body::apply_impulse(const kmVec3& impulse) {
    kmVec3 final;
    kmVec3Scale(&final, &impulse, inv_mass_);
    kmVec3Add(&linear_velocity_, &linear_velocity_, &final);
}

void Body::apply_local_impulse(const kmVec3& impulse) {
    kmVec3 rotated;
    kmQuaternionMultiplyVec3(&rotated, &rotation_, &impulse);
    apply_impulse(rotated);
}

void Body::apply_torque(const kmVec3& force) {
    kmVec3Add(&torque_, &torque_, &force);
}

void Body::apply_angular_impulse(const kmVec3& force) {
    kmVec3Add(&angular_velocity_, &angular_velocity_, &force);
}

kmVec3 Body::linear_velocity_at(const kmVec3& point) {
    // Given a position the linear velocity is the linear velocity of the center of mass
    // plus, the additional velocity at the point which is the cross product of the offset
    // from the center of mass and the angular velocity. Apparently.

    auto vel = linear_velocity();
    auto avel = angular_velocity();
    auto pos = center_of_mass();

    kmVec3 offset;
    kmVec3Subtract(&offset, &point, &pos);

    kmVec3 additional;
    kmVec3Cross(&additional, &avel, &offset);
    kmVec3Add(&vel, &vel, &additional);

    return vel;
}


void Body::update(double dt) {
    kmVec3 additional_linear;
    kmVec3Scale(&additional_linear, &force_, inv_mass_ * dt);

    kmVec3 additional_angular;
    kmVec3MultiplyMat3(&additional_angular, &torque_, &inverse_inertia_tensor_WS_);
    kmVec3Scale(&additional_angular, &additional_angular, dt);

    // Add adjusted torque and linear force to the velocities
    kmVec3Add(&linear_velocity_, &linear_velocity_, &additional_linear);
    kmVec3Add(&angular_velocity_, &angular_velocity_, &additional_angular);

    // Apply damping
    kmVec3Scale(&linear_velocity_, &linear_velocity_, 1.0f / (1.0f + dt * linear_damping_));
    kmVec3Scale(&angular_velocity_, &angular_velocity_, 1.0f / (1.0f + dt * angular_damping_));

    // Add the linear velocity to the position
    kmVec3 to_move;
    kmVec3Scale(&to_move, &linear_velocity_, dt);
    kmVec3Add(&position_, &position_, &to_move);

    kmQuaternion final_rotation;
    kmVec3 to_rotate;
    kmVec3Scale(&to_rotate, &angular_velocity_, dt);

    kmQuaternionFill(&final_rotation, to_rotate.x, to_rotate.y, to_rotate.z, 0);
    kmQuaternionMultiply(&final_rotation, &final_rotation, &rotation_);
    kmQuaternionAdd(&rotation_, &rotation_, &final_rotation);
    kmQuaternionNormalize(&rotation_, &rotation_);

    kmVec3Fill(&force_, 0, 0, 0);
    kmVec3Fill(&torque_, 0, 0, 0);

    recalc_world_inertia();

    /*return;


    kmVec3 v = linear_velocity_;
    kmVec3 t = angular_velocity_;

    kmVec3 add_force;
    //v += dt * (inv_mass_ * force_);
    kmVec3Scale(&add_force, &force_, inv_mass_);
    kmVec3Scale(&add_force, &add_force, dt);
    kmVec3Add(&v, &v, &add_force);

    kmVec3 add_torque;
    //t += dt * (inv_inertia_ * torque_);
    kmVec3MultiplyMat3(&add_torque, &torque_, &inverse_inertia_tensor_WS_);
    kmVec3Scale(&add_torque, &add_torque, dt);
    kmVec3Add(&t, &t, &add_torque);

    // Apply damping

    //v *= 1.0f / (1.0f + dt * linear_damping_);
    kmVec3Scale(&v, &v, 1.0f / (1.0f + dt * linear_damping_));

    //t *= 1.0f / (1.0f + dt * angular_damping_);
    kmVec3Scale(&t, &t, 1.0f / (1.0f + dt * angular_damping_));

    kmVec3 mov;
    kmVec3Scale(&mov, &v, dt);

    assert(!isnan(mov.x));
    assert(!isnan(mov.y));
    assert(!isnan(mov.z));

    kmVec3Add(&position_, &position_, &mov);

    kmVec3 tx;
    kmVec3Scale(&tx, &t, dt);

    assert(!isnan(tx.x));
    assert(!isnan(tx.y));
    assert(!isnan(tx.z));

    kmQuaternion new_rot;
    kmQuaternionFill(&new_rot, tx.x, tx.y, tx.z, 0.0);

    kmQuaternion orientation = rotation_;
    kmQuaternionMultiply(&orientation, &new_rot, &orientation);
    kmQuaternionAdd(&rotation_, &rotation_, &orientation);
    kmQuaternionNormalize(&rotation_, &rotation_);

    linear_velocity_ = v;
    angular_velocity_ = t;

    kmVec3Fill(&force_, 0, 0, 0);
    kmVec3Fill(&torque_, 0, 0, 0);

    recalc_world_inertia();

    assert(!isnan(position_.x));
    assert(!isnan(position_.y));
    assert(!isnan(position_.z));
    assert(!isnan(rotation_.x));
    assert(!isnan(rotation_.y));
    assert(!isnan(rotation_.z));  */
}

void Body::set_shape_box(kmVec3 dimensions) {
    shape_ = RIGID_BODY_SHAPE_BOX;
    shape_dimensions_ = dimensions;
    calculate_inertia();
}

void Body::set_shape_sphere(float diameter) {
    shape_ = RIGID_BODY_SHAPE_SPHERE;
    shape_dimensions_ = { diameter, diameter, diameter };
    calculate_inertia();
}

void Body::recalc_world_inertia() {
    kmMat3 world_rotation;
    kmMat3FromRotationQuaternion(&world_rotation, &rotation_);

    // ??? Should this be the inverse of the world rotation?
    kmMat3MultiplyMat3(&inverse_inertia_tensor_WS_, &inverse_inertia_tensor_, &world_rotation);
}

void Body::calculate_inertia() {
    const float oneOverTwelve = 1.0 / 12.0;
    float x2 = shape_dimensions_.x * shape_dimensions_.x;
    float y2 = shape_dimensions_.y * shape_dimensions_.y;
    float z2 = shape_dimensions_.z * shape_dimensions_.z;

    switch(shape_) {
        case RIGID_BODY_SHAPE_BOX: {
            std::fill(inertia_tensor_.mat, inertia_tensor_.mat + 9, 0);
            inertia_tensor_.mat[0] = oneOverTwelve * mass_ * (y2 + z2);
            inertia_tensor_.mat[4] = oneOverTwelve * mass_ * (x2 + z2);
            inertia_tensor_.mat[8] = oneOverTwelve * mass_ * (x2 + y2);
        } break;
        default:
            throw std::logic_error("Invalid shape type");
    }

    kmMat3Inverse(&inverse_inertia_tensor_, &inertia_tensor_);
    recalc_world_inertia();
}

void Body::set_mass(float value) {
    mass_ = value;
    inv_mass_ = 1.0 / mass_;

    calculate_inertia();
}
