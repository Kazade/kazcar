#include "kazcar.h"
#include "kazcar_private.h"
#include "car.h"
#include "route.h"
#include "ai_controller.h"

WorldID kc_world_create() {
    return create_world();
}

void kc_world_set_gravity(WorldID world_id, const kmVec3* gravity) {
    World* world = find_world(world_id);
    world->set_gravity(gravity);
}

void kc_world_step(WorldID world_id, double dt) {
    World* world = find_world(world_id);
    world->step(dt);
}

void kc_world_end_frame(WorldID world_id) {
    World* world = find_world(world_id);
    world->end_frame();
}

void kc_world_add_triangle(WorldID world_id, const kmVec3* v1, const kmVec3* v2, const kmVec3* v3) {
    World* world = find_world(world_id);
    world->add_triangle(v1, v2, v3);
}

void kc_world_add_triangle_v(WorldID world_id, const kmVec3* verts) {
    kc_world_add_triangle(world_id, &verts[0], &verts[1], &verts[2]);
}

void kc_world_set_render_callback(WorldID world_id, DebugRenderCallback callback, void *user_data) {
    using namespace std::placeholders;

    World* world = find_world(world_id);
    world->set_render_callback(
        std::bind(callback, _1, _2, _3, _4, _5, user_data)
    );
}

void kc_world_destroy(WorldID world) {
    destroy_world(world);
}

CarID kc_car_create(WorldID world_id) {
    return create_car(world_id);
}

void kc_car_set_position(CarID car_id, const kmVec3 *position) {
    Car* car = find_car(car_id);
    car->set_position(*position);
}

void kc_car_set_rotation(CarID car_id, const kmQuaternion *rotation) {
    Car* car = find_car(car_id);
    car->body()->set_rotation(*rotation);
}

kmVec3 kc_car_position(CarID car_id) {
    Car* car = find_car(car_id);
    return car->body()->position();
}

kmQuaternion kc_car_rotation(CarID car_id) {
    Car* car = find_car(car_id);
    return car->body()->rotation();
}

kmVec3 kc_car_wheel_position(CarID car_id, uint8_t wheel) {
    Car* car = find_car(car_id);
    return car->wheel_position(wheel);
}

kmQuaternion kc_car_wheel_rotation(CarID car_id, uint8_t wheel) {
    Car* car = find_car(car_id);
    return car->wheel_rotation(wheel);
}

kmVec3 kc_car_wheel_right(CarID car_id, uint8_t wheel) {
    Car* car = find_car(car_id);
    kmVec3 up;

    // Try to get the normal of any existing wheel collision
    if(!kc_car_wheel_collision_normal(car_id, wheel, &up)) {
        // If there was no collision, use the body's up vector
        up = car->body()->up();
    }

    return car->wheel_right_axis(wheel, up);
}

kmVec3 kc_car_wheel_forward(CarID car_id, uint8_t wheel) {
    Car* car = find_car(car_id);

    kmVec3 up;

    // Try to get the normal of any existing wheel collision
    if(!kc_car_wheel_collision_normal(car_id, wheel, &up)) {
        // If there was no collision, use the body's up vector
        up = car->body()->up();
    }

    return car->wheel_forward_axis(wheel, up);
}

float kc_car_axel_width(CarID car_id) {
    Car* car = find_car(car_id);
    return car->axel_width();
}

float kc_car_axel_separation(CarID car_id) {
    Car* car = find_car(car_id);
    return car->axel_separation();
}

float kc_car_max_suspension_length(CarID car_id) {
    Car* car = find_car(car_id);
    return car->max_suspension_length();
}

float kc_car_current_suspension_length(CarID car_id, uint8_t wheel) {
    Car* car = find_car(car_id);
    return car->current_suspension_length(wheel);
}

void kc_car_turn_left(CarID car_id) {
    Car* car = find_car(car_id);
    car->turn_left();
}

void kc_car_turn_right(CarID car_id) {
    Car* car = find_car(car_id);
    car->turn_right();
}

float kc_car_wheel_radius(CarID car_id) {
    Car* car = find_car(car_id);
    return car->tire_radius();
}

void kc_car_accelerate(CarID car_id) {
    Car* car = find_car(car_id);
    car->accelerate();
}

void kc_car_brake(CarID car_id) {
    Car* car = find_car(car_id);
    car->brake();
}

void kc_car_handbrake(CarID car_id) {
    Car* car = find_car(car_id);
    car->handbrake();
}

bool kc_car_wheel_collision_normal(CarID car_id, uint8_t wheel, kmVec3* result) {
    Car* car = find_car(car_id);

    auto data = car->wheel_collision_data(wheel);
    if(!data) {
        return false;
    }

    kmVec3Assign(result, &data->normal);
    return true;
}


RouteID kc_route_create(WorldID world_id) {
    return create_route(world_id);
}

void kc_route_push_waypoint(RouteID route_id, const kmVec3* location, const float radius) {
    Route* route = find_route(route_id);
    route->push_waypoint(*location, radius);
}

uint32_t kc_route_iteration_count(RouteID route_id) {
    Route* route = find_route(route_id);
    return route->iterations();
}

void kc_route_set_iterations(RouteID route_id, uint32_t iterations) {
    Route* route = find_route(route_id);
    route->set_iterations(iterations);
}

uint32_t kc_route_waypoint_count(RouteID route_id) {
    Route* route = find_route(route_id);
    return route->waypoint_count();
}

void kc_route_destroy(RouteID route_id) {
    destroy_route(route_id);
}

AIControllerID kc_ai_controller_create(WorldID world_id, RouteID route, AILevel level) {
    return create_ai_controller(world_id, route, level);
}

void kc_ai_controller_set_car(AIControllerID boid_id, CarID car_id) {
    AIController* controller = find_ai_controller(boid_id);
    controller->set_car(car_id);
}

void kc_ai_controller_start(AIControllerID boid_id) {
    AIController* controller = find_ai_controller(boid_id);
    controller->start();
}

void kc_ai_controller_pause(AIControllerID boid_id) {
    AIController* controller = find_ai_controller(boid_id);
    controller->pause();
}

void kc_ai_controller_resume(AIControllerID boid_id) {
    AIController* controller = find_ai_controller(boid_id);
    controller->resume();
}

void kc_ai_controller_reset(AIControllerID boid_id) {

}

void kc_ai_controller_update(AIControllerID boid_id, double dt) {

}

void kc_ai_controller_destroy(AIControllerID boid_id) {

}
