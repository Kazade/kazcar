
#include "route.h"

Route::Route(RouteID id, World* world):
    id_(id),
    world_(world) {

}

void Route::push_waypoint(const kmVec3& position, float radius) {
    waypoints_.push_back(Waypoint{position, radius});
}

uint32_t Route::waypoint_count() const {
    return waypoints_.size();
}
