#pragma once

#include <kaztest/kaztest.h>
#include "kazcar/kazcar_private.h"

class TestCollisions : public TestCase {
public:
    void test_intersect_sphere_line_segment_start() {
        std::vector<CollisionInfo> collisions;

        Sphere sphere;
        sphere.radius = 0.5;
        kmVec3Fill(&sphere.position, -0.5, 0, 0);

        LineSegment line;
        kmVec3Fill(&line.p1, 0.0, 0.0, 0.0);
        kmVec3Fill(&line.p2, 1.0, 0.0, 0.0);

        assert_true(intersect_sphere_line_segment(sphere, line, collisions));

        kmVec3Fill(&sphere.position, -0.6, 0, 0);
        assert_false(intersect_sphere_line_segment(sphere, line, collisions));
    }

    void test_intersect_sphere_line_segment_end() {
        std::vector<CollisionInfo> collisions;

        Sphere sphere;
        sphere.radius = 0.5;
        kmVec3Fill(&sphere.position, 1.5, 0, 0);

        LineSegment line;
        kmVec3Fill(&line.p1, 0.0, 0.0, 0.0);
        kmVec3Fill(&line.p2, 1.0, 0.0, 0.0);

        assert_true(intersect_sphere_line_segment(sphere, line, collisions));

        kmVec3Fill(&sphere.position, 1.6, 0, 0);
        assert_false(intersect_sphere_line_segment(sphere, line, collisions));
    }

    void test_intersect_sphere_line_segment_middle() {
        std::vector<CollisionInfo> collisions;

        Sphere sphere;
        sphere.radius = 0.5;
        kmVec3Fill(&sphere.position, 0, 0.5, 0);

        LineSegment line;
        kmVec3Fill(&line.p1, 0.0, 0.0, 0.0);
        kmVec3Fill(&line.p2, 1.0, 0.0, 0.0);

        assert_true(intersect_sphere_line_segment(sphere, line, collisions));

        kmVec3Fill(&sphere.position, 0, 0.6, 0);
        assert_false(intersect_sphere_line_segment(sphere, line, collisions));
    }

    void test_unit_sphere_triangle_face() {
        std::vector<CollisionInfo> collisions;
        Triangle triangle;
        Sphere sphere;
        sphere.radius = 0.5;

        triangle.vertices[0] = { -1, 0, 0 };
        triangle.vertices[1] = { 0, 0, 1 };
        triangle.vertices[2] = { 1, 0, 0 };
        triangle.recalc_normal();

        sphere.position = { 0, 0.5, 0.25 };

        bool ret = collide_triangle_and_sphere(triangle, sphere, collisions);
        assert_true(ret);

        sphere.position = { 0, 0.6, 0.25 };
        ret = collide_triangle_and_sphere(triangle, sphere, collisions);
        assert_false(ret);
    }

    void test_unit_sphere_triangle_corner() {
        std::vector<CollisionInfo> collisions;
        Triangle triangle;
        Sphere sphere;
        sphere.radius = 0.5;

        triangle.vertices[0] = { -1, 0, 0 };
        triangle.vertices[1] = { 0, 0, 1 };
        triangle.vertices[2] = { 1, 0, 0 };
        triangle.recalc_normal();

        sphere.position = { -1.5, 0, 0 };

        bool ret = collide_triangle_and_sphere(triangle, sphere, collisions);
        assert_true(ret);

        sphere.position = { -1.6, 0, 0 };
        ret = collide_triangle_and_sphere(triangle, sphere, collisions);
        assert_false(ret);
    }

    void test_unit_sphere_triangle_edge() {
        std::vector<CollisionInfo> collisions;
        Triangle triangle;
        Sphere sphere;
        sphere.radius = 0.5;

        triangle.vertices[0] = { -1, 0, 0 };
        triangle.vertices[1] = { 0, 0, 1 };
        triangle.vertices[2] = { 1, 0, 0 };
        triangle.recalc_normal();

        sphere.position = { 0, 0, -0.5 };

        bool ret = collide_triangle_and_sphere(triangle, sphere, collisions);
        assert_true(ret);

        sphere.position = { 0, 0, -0.6 };
        ret = collide_triangle_and_sphere(triangle, sphere, collisions);
        assert_false(ret);
    }

    void test_unit_sphere_triangle_same_plane_miss() {
        std::vector<CollisionInfo> collisions;
        Triangle triangle;
        Sphere sphere;
        sphere.radius = 0.5;

        triangle.vertices[0] = { -1, 0, 0 };
        triangle.vertices[1] = { 0, 0, 1 };
        triangle.vertices[2] = { 1, 0, 0 };
        triangle.recalc_normal();

        sphere.position = { 0, 0, -10.0 };

        bool ret = collide_triangle_and_sphere(triangle, sphere, collisions);
        assert_false(ret);
    }

    void test_triangle_obb_intersection() {
        std::vector<CollisionInfo> collisions;

        OBB obb;
        obb.dimensions = { 1, 1, 1 };
        obb.position = { 0, 0, -0.5 };
        kmQuaternionFill(&obb.rotation, 0, 0, 0, 1);

        Triangle triangle;
        triangle.vertices[0] = { -1, 0, 0 };
        triangle.vertices[1] = { 0, 0, 1 };
        triangle.vertices[2] = { 1, 0, 0 };
        triangle.recalc_normal();

        bool ret = collide_triangle_and_obb(triangle, obb, collisions);
        assert_true(ret);

        obb.position = { 0, 0, -1.1 };
        ret = collide_triangle_and_obb(triangle, obb, collisions);
        assert_false(ret);
    }

    void test_triangle_ray_intersection() {
        std::vector<CollisionInfo> collisions;

        Triangle triangle;
        triangle.vertices[0] = { -1, 0, 0 };
        triangle.vertices[1] = { 0, 0, 1 };
        triangle.vertices[2] = { 1, 0, 0 };
        triangle.recalc_normal();

        kmRay3 ray;
        kmRay3Fill(&ray, 0, 1, 0, 0, -1, 0);

        bool ret = collide_triangle_and_ray(triangle, ray, collisions);
        assert_true(ret);

        assert_equal(1, collisions.size());
        assert_close(1.0, collisions[0].intersection_depth, 0.0001);
        assert_true(kmVec3AreEqual(&KM_VEC3_ZERO, &collisions[0].intersection));

        kmRay3Fill(&ray, 5, 1, 0, 0, -1, 0);
        ret = collide_triangle_and_ray(triangle, ray, collisions);
        assert_false(ret);
        assert_true(collisions.empty());

        // Ray pointing upward, but with a starting point intersecting
        kmRay3Fill(&ray, 0, 0, 0, 0, 1, 0);
        ret = collide_triangle_and_ray(triangle, ray, collisions);
        assert_true(ret); // Should collide

        assert_close(collisions.front().intersection_depth, 0, 0.0001);
        assert_true(kmVec3AreEqual(&KM_VEC3_ZERO, &collisions[0].intersection));
        assert_true(kmVec3AreEqual(&KM_VEC3_POS_Y, &collisions[0].normal));

        kmRay3Fill(&ray, 0, 0.1, 0, 0, 1, 0);
        ret = collide_triangle_and_ray(triangle, ray, collisions);
        assert_false(ret); // Shouldn't collide
    }

    void test_obb_obb_intersection() {
        std::vector<CollisionInfo> collisions;

        OBB obb1, obb2;
        obb1.position = { 0, 0, 0 };
        obb1.dimensions = {1, 1, 1};
        obb1.rotation = { 0, 0, 0, 1 };

        obb2.position = { 1, 0, 0 };
        obb2.dimensions = { 1, 1, 1 };
        obb2.rotation = { 0, 0, 0, 1 };


        bool ret = collide_obb_and_obb(obb1, obb2, collisions);
        assert_true(ret);

        obb2.position = { 1.1, 0, 0 };

        ret = collide_obb_and_obb(obb1, obb2, collisions);
        assert_false(ret);

        obb1.position = { 0.1, 0, 0 };

        ret = collide_obb_and_obb(obb1, obb2, collisions);
        assert_true(ret);
    }
};
