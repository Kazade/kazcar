#ifndef TEST_API_H
#define TEST_API_H

#include <kaztest/kaztest.h>
#include "kazcar/kazcar.h"

class APITest : public TestCase {
private:
    WorldID world_;
public:
    void set_up() {
        world_ = kc_world_create();
    }

    void tear_down() {
        kc_world_destroy(world_);
    }

    void test_wheel_position() {
        CarID car = kc_car_create(world_);

        auto axel_width = kc_car_axel_width(car);
        auto axel_sep = kc_car_axel_separation(car);

        auto sus_length = kc_car_current_suspension_length(car, 0);
        auto wheel_pos = kc_car_wheel_position(car, 0);

        auto expected_position = kmVec3{-axel_width / 2, -sus_length, -axel_sep / 2};
        assert_true(kmVec3AreEqual(&expected_position, &wheel_pos));

        //kc_car_destroy(car);
    }
};

#endif // TEST_API_H

