#ifndef TEST_ROUTE_H
#define TEST_ROUTE_H

#include <kaztest/kaztest.h>
#include <kazcar/kazcar.h>

class RouteTest : public TestCase {
private:
    WorldID wid;
    RouteID rid;

public:
    void set_up() {
        wid = kc_world_create();
        rid = kc_route_create(wid);
    }

    void tear_down() {
        kc_route_destroy(rid);
        kc_world_destroy(wid);
    }

    void test_pushing_waypoints() {
        kmVec3 wp1 = { 0, 0, 0 };
        kmVec3 wp2 = { 5, 0, 0 };

        assert_equal(0, kc_route_waypoint_count(rid));
        kc_route_push_waypoint(rid, &wp1, 1.0);
        assert_equal(1, kc_route_waypoint_count(rid));
        kc_route_push_waypoint(rid, &wp2, 1.0);
        assert_equal(2, kc_route_waypoint_count(rid));
    }

    void test_setting_looping() {
        assert_equal(1, kc_route_iteration_count(rid));
        kc_route_set_iterations(rid, 5);
        assert_equal(5, kc_route_iteration_count(rid));
    }

};

#endif // TEST_ROUTE_H

