# kazcar

[![build status](https://ci.gitlab.com/projects/6305/status.png?ref=master)](https://ci.gitlab.com/projects/6305?ref=master)

This is my attempt at a really basic car racing physics engine. It's intended to be very specific
there will only be triangles making up the world, cars, and simple sphere/box objects for road furniture etc.

The car physics will use 4 downward rays for the wheels, I'm experimenting using a half-ellipsoid for the car
shell for faster collisions but might revert back to a box.

The only dependency is kazmath. The library has a C-API so it can be used easily from other languages (e.g. Python)
but the library itself is C++11.